# latex

## Installation

1. Download MacTeX *Smaller Download* from [https://tug.org/mactex/](https://tug.org/mactex/)

2. Open a terminal and do this:

  ```bash
  sudo tlmgr update --all --self
  sudo tlmgr install collection-langkorean
  ```

3. Create a sample TeX file `sample.tex`:

  ```latex
  \documentclass[a4paper]{article}
  \usepackage{kotex}
  \begin{document}
  안녕하세요.
  \end{document}
  ```

4. In the terminal, compile `sample.tex`:

  ```bash
  pdflatex sample.tex
  ```
